import argparse

def get_args():
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", type=str,
                    help="path to input image")

    ap.add_argument("-s", "--show-images", "--show_images", dest='show_images', action='store_true')
    ap.set_defaults(show_images=False)

    ap.add_argument("-w", "--width", type=int, default=320,
                    help="nearest multiple of 32 for resized width")
    ap.add_argument("-e", "--height", type=int, default=320,
                    help="nearest multiple of 32 for resized height")
    return vars(ap.parse_args())