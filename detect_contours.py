from genericpath import isdir
import numpy as np
import cv2
import matplotlib.pyplot as plt
from pytesseract import Output
import argparse
from time import perf_counter
import threading
import os
from os import listdir
from os.path import isfile, join

#
#from __future__ import print_function
import cv2 as cv
import random as rng

#from skimage.filters import threshold_local
from PIL import Image

def opencv_resize(image, ratio):
    width = int(image.shape[1] * ratio)
    height = int(image.shape[0] * ratio)
    dim = (width, height)
    return cv2.resize(image, dim, interpolation = cv2.INTER_AREA)

def plot_rgb(image):
    plt.figure(figsize=(16,10))
    plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
    return plt.show()

def plot_gray(image):
    #print("nop")
    plt.figure(figsize=(16,10))
    plt.imshow(image, cmap='Greys_r')
    return plt.show()

# approximate the contour by a more primitive polygon shape
def approximate_contour(contour):
    peri = cv2.arcLength(contour, True)
    return cv2.approxPolyDP(contour, 0.032 * peri, True)

def get_receipt_contour(contours):    
    # loop over the contours
    for c in contours:
        approx = approximate_contour(c)
        # if our approximated contour has four points, we can assume it is receipt's rectangle
        if len(approx) == 4:
            return approx

def contour_to_rect(contour, resize_ratio):
    pts = contour.reshape(4, 2)
    rect = np.zeros((4, 2), dtype = "float32")
    # top-left point has the smallest sum
    # bottom-right has the largest sum
    s = pts.sum(axis = 1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    # compute the difference between the points:
    # the top-right will have the minumum difference 
    # the bottom-left will have the maximum difference
    diff = np.diff(pts, axis = 1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    return rect / resize_ratio

def wrap_perspective(img, rect):
    # unpack rectangle points: top left, top right, bottom right, bottom left
    (tl, tr, br, bl) = rect
    # compute the width of the new image
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    # compute the height of the new image
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    # take the maximum of the width and height values to reach
    # our final dimensions
    maxWidth = max(int(widthA), int(widthB))
    maxHeight = max(int(heightA), int(heightB))
    # destination points which will be used to map the screen to a "scanned" view
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype = "float32")
    # calculate the perspective transform matrix
    M = cv2.getPerspectiveTransform(rect, dst)
    # warp the perspective to grab the screen
    return cv2.warpPerspective(img, M, (maxWidth, maxHeight))

def bw_scanner(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # T = threshold_local(gray, 21, offset = 5, method = "gaussian")
    # return (gray > T).astype("uint8") * 255
    return gray

def measure(name: str, action):
    start = perf_counter()
    res = action()
    print("%s time %.f" % (name, perf_counter() - start))
    return res


def main(args):

    file_name = args.input
    #img = Image.open(file_name)
    #img.thumbnail((800,800), Image.ANTIALIAS)
    #img

    image = cv2.imread(file_name)
    # Downscale image as finding receipt contour is more efficient on a small image
    resize_ratio = 500 / image.shape[0]
    original = image.copy()
    image = measure("Resize", lambda: opencv_resize(image, resize_ratio))

    # Convert to grayscale for further processing
    #plot_gray(cv2.Canny(image, 100, 200, apertureSize=3))
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #plot_gray(gray)

    # Get rid of noise with Gaussian Blur filter
    #plot_gray(cv2.Canny(gray, 100, 200, apertureSize=3))
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    #plot_gray(blurred)

    # Detect white regions
    plot_gray(cv2.Canny(blurred, 10, 50, apertureSize=3))
    rectKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 9))
    dilated = cv2.dilate(blurred, rectKernel)
    plot_gray(dilated)

    edged = cv2.Canny(blurred, 100, 200, apertureSize=3)
    #edged = cv2.Canny(dilated, 100, 200, apertureSize=3)
    plot_gray(edged)

    # Detect all contours in Canny-edged image
    contours, hierarchy = cv2.findContours(edged, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    image_with_contours = cv2.drawContours(image.copy(), contours, -1, (0,255,0), 3)
    plot_rgb(image_with_contours)

    largest_contours = sorted(contours, key = cv2.contourArea, reverse = True)[:10]
    image_with_largest_contours = cv2.drawContours(image.copy(), largest_contours, -1, (0,255,0), 3)
    plot_rgb(image_with_largest_contours)

    get_receipt_contour(largest_contours)

    receipt_contour = get_receipt_contour(largest_contours)
    image_with_receipt_contour = cv2.drawContours(image.copy(), [receipt_contour], -1, (0, 255, 0), 2)
    plot_rgb(image_with_receipt_contour)

    scanned = wrap_perspective(original.copy(), contour_to_rect(receipt_contour, resize_ratio))
    plt.figure(figsize=(16,10))
    plt.imshow(scanned)

    result = bw_scanner(scanned)
    plot_gray(result)

    output = Image.fromarray(result)
    output.save('result/result.png')

    # Get OCR text
    d = pytesseract.image_to_data(result, output_type=Output.DICT)
    n_boxes = len(d['level'])
    boxes = cv2.cvtColor(result.copy(), cv2.COLOR_BGR2RGB)
    for i in range(n_boxes):
        (x, y, w, h) = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])    
        boxes = cv2.rectangle(boxes, (x, y), (x + w, y + h), (0, 255, 0), 2)
        
    #plot_rgb(boxes)

    # config = ("-l swe --oem 1 --psm 3")
    # extracted_text = pytesseract.image_to_string(result, config=config)
    # #extracted_text = pytesseract.image_to_string(result)
    # print(extracted_text)

    print("Done..")

parser = argparse.ArgumentParser(description="Preprocess img")
parser.add_argument('--input', help='Path to input image.')
args = parser.parse_args()

#main(args)

rng.seed(12345)

def thresh_update(t1, t2, blur_kernel, equalize=True):
    print("Update canny %i, %i, %i." % (t1, t2, blur_kernel))
    # Detect edges using Canny
    #src_blurred = cv.blur(src_gray, (blur_kernel, blur_kernel))
    src_gray = thread_data.src_gray
    dst = src_gray
    if equalize:
        dst = cv.equalizeHist(src_gray)
        cv.imshow('Equalized Image', dst)

    if blur_kernel % 2 == 0:
        blur_kernel += 1 # GaussianBlur requires odd kernel
    src_blurred = cv.GaussianBlur(dst, (blur_kernel, blur_kernel), 0)
    #src_blurred = cv.bilateralFilter(src_gray, 5, blur_kernel, blur_kernel)
    
    cv.imshow('Blurred', src_blurred)

    # ret3, src_thresh = cv.threshold(src_blurred, 0, 255, cv.THRESH_BINARY+cv.THRESH_OTSU)
    # cv.imshow('THRESH_OTSU' ,src_thresh)

    # sharpen_kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
    # sharpen = cv2.filter2D(src_blurred, -1, sharpen_kernel)
    # cv.imshow('sharpen_kernel' ,sharpen)
    
    canny_output = cv.Canny(src_blurred , t1, t2)
    # Find contours
    contours, hierarchy = cv.findContours(canny_output, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    # Draw contours
    drawing = np.zeros((canny_output.shape[0], canny_output.shape[1], 3), dtype=np.uint8)
    for i in range(len(contours)):
        color = (rng.randint(0,256), rng.randint(0,256), rng.randint(0,256))
        cv.drawContours(drawing, contours, i, color, 2, cv.LINE_8, hierarchy, 0)
    # Show in a window
    cv.imshow('Contours', drawing)

def thresh_update_thread_data():
    thresh_update(thread_data.threshold1, thread_data.threshold2, thread_data.blur_kernel_sz, thread_data.equalize)

def thresh_callback1(val):
    thread_data.threshold1 = val
    thresh_update_thread_data()

def thresh_callback2(val):
    thread_data.threshold2 = val     
    thresh_update_thread_data()

def blur_callback(val):
    thread_data.blur_kernel_sz = val
    thresh_update_thread_data()

def equalize_callback(val):
    print("Eaualize %s " % val)
    thread_data.equalize = val == 1
    thresh_update_thread_data()

def plt_histogram(img):
    # create border mask
    
    mask_frame = np.full(img.shape[:2], fill_value=255, dtype=np.uint8)
    mask_center = np.zeros(img.shape[:2], dtype=np.uint8)
    border_percent = 0.15
    mask_x_size = int(border_percent * img.shape[0])
    mask_x_end = img.shape[0] - mask_x_size

    mask_y_size = int(border_percent * img.shape[1])
    mask_y_end = img.shape[1] - mask_y_size

    mask_frame[mask_x_size:mask_x_end, mask_y_size:mask_y_end] = 0
    mask_center[mask_x_size:(img.shape[0] - mask_x_size), mask_y_size:mask_y_end] = 255
    
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    masked_frame_img = cv.bitwise_and(img_gray, img_gray, mask=mask_frame)    
    #dominant_color("FRAME GRAYSCALE", cv2.cvtColor(masked_frame_img, cv2.COLOR_BGR2GRAY))

    masked_center_img = cv.bitwise_and(img_gray, img_gray, mask=mask_center)
    #dominant_color("CENTER GRAYSCALE", cv2.cvtColor(masked_center_img, cv2.COLOR_BGR2GRAY))
    # Calculate histogram with mask and without mask
    # Check third argument for mask
    
    mask_center_size = (mask_x_end - mask_x_size) * (mask_y_end - mask_y_size)
    hist_center = np.bincount(masked_center_img.ravel(), minlength=256) # Count occurences of 0-255 gray color values
    hist_center[0] -= ((img.shape[0] * img.shape[1]) - mask_center_size) # Remove black mask color from from result
    
    hist_mask = np.bincount(masked_frame_img.ravel(), minlength=256)
    hist_mask[0] -= mask_center_size
    
    # hist_center = cv.calcHist([masked_center_img], [0], None, [256], [1, 256])
    # hist_mask = cv.calcHist([img], [0], mask, [256], [1, 256])
    plt.subplot(221), plt.imshow(img, 'gray')
    plt.subplot(222), plt.imshow(masked_center_img, 'gray')
    plt.subplot(223), plt.imshow(masked_frame_img, 'gray')
    plt.subplot(224), plt.plot(hist_center), plt.plot(hist_mask)    
    #plt.subplot(224), plt.plot(hist_full)    
    plt.xlim([0, 256])
    plt.show()

def dominant_color(name: str, img_gray):
    colors_accumlated = np.bincount(img_gray.ravel(), minlength=256)

    data = np.reshape(img_gray, (-1, 3))
    print(data.shape)
    data = np.float32(data)

    #np.bincount(data)

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    flags = cv2.KMEANS_RANDOM_CENTERS
    compactness, labels, centers = cv2.kmeans(data, 1, None, criteria, 10, flags)

    print('Dominant color %s is: bgr(%s)' % (name, centers[0].astype(np.int32)))

def process_img_file(input):
    src = measure("Read img", lambda: cv.imread(input))
    if src is None:
        print('Could not open or find the image:', input)
        exit(0)
    resize_ratio = 500 / src.shape[0]
    src = measure("Resize", lambda: opencv_resize(src, resize_ratio))

    plt_histogram(src)

    dominant_color("Orginal", src)

    # Convert image to gray and blur it
    thread_data.src_gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)
    #src_gray = cv.blur(src_gray, (3,3))
    # Create Window
    source_window = 'Source'
    cv.namedWindow(source_window)
    cv.imshow(source_window, src)
    max_thresh = 255
    thresh = 20 # initial threshold
    blur_kernel_sz = 3

    
    thread_data.threshold1 = thresh
    thread_data.threshold2 = thresh * 4
    thread_data.blur_kernel_sz = blur_kernel_sz

    cv.createTrackbar('Canny Thresh 1:', source_window, thresh, max_thresh, thresh_callback1)
    cv.createTrackbar('Canny Thresh 2:', source_window, thresh, max_thresh, thresh_callback2)
    cv.createTrackbar('Blur kernel size:', source_window, blur_kernel_sz, blur_kernel_sz*20, blur_callback)
    cv.createTrackbar('Equalize OFF/ON', source_window, 0, 1, equalize_callback)
    thresh_update(thread_data.threshold1, thread_data.threshold2, blur_kernel_sz)
    cv.waitKey()
    print("Key pressed")

isDir = os.path.isdir(args.input)

thread_data = threading.local()
thread_data.src_gray = None

if isDir:
    files = [join(args.input, f) for f in listdir(args.input) if isfile(join(args.input, f))]
    files = filter( lambda f: f.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif')), files)

    for f in files:
        process_img_file(f)
else:
    process_img_file(args.input)
