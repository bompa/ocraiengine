
from matplotlib.pyplot import text
import ocr.text_extract as ocr
import ocr.preprocess as preprocess
import cv2
import difflib
from difflib import SequenceMatcher
import images.image_helpers as images

def test_ocr_output():
    file = 'bauhaus'
    image = cv2.imread('resources/images/{0}.jpg'.format(file), 0)
    preProcessedImage = preprocess.process_image_for_ocr(image)

    res, textResult = ocr.run(preProcessedImage, False)

    textResult = ocr.clean_text(textResult)

    f = open('resources/images/text_result_google/{0}.txt'.format(file), 'r')
    textExpected = f.read()

    # diff = open('resources/images/text_result_google/{0}.diff'.format(file), 'w')
    # for line in difflib.unified_diff(textResult, textExpected):
    #     diff.write(line),

    m = SequenceMatcher(None, textResult, textExpected)
    assert m.ratio() > 0.9

def test_ocr_preprocess():
    file = 'bauhaus'
    image = cv2.imread('resources/images/{0}.jpg'.format(file), cv2.CV_32F)
#preprocess.get_grayscale(cv2.imread(args["image"], cv2.CV_32F))
    pp1 = preprocess.get_grayscale(image.copy())
    images.plot_rgb(pp1, "Grayscale")

    pp2 = preprocess.remove_noise(image.copy())
    images.plot_rgb(pp2, "Rem noise")

    pp2 = preprocess.remove_noise(pp1.copy())
    images.plot_rgb(pp2, "Grayscale Rem noise")

    #pp3 = preprocess.thresholding(image.copy())
    #images.plot_rgb(pp3, "Tresholding")

    pp3 = preprocess.thresholding(pp2.copy())
    images.plot_rgb(pp3, "Tresholding Grayscale Rem noise")

    
