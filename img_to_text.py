from imutils.object_detection import non_max_suppression
import img_to_text_args as app_args
import cv2
import images.image_helpers as images
import ocr.preprocess as preprocess
import ocr.text_extract as ocr
import text_detection.detect_text as detect_text
from util.timer import Timer
from PIL import Image
import os

args = app_args.get_args()
t = Timer()

preprocess.deskew(args["image"])
#print("Deskew done..")

image = cv2.imread(args["image"])

image = preprocess.process_image_for_ocr_file(args["image"])


showImages = args["show_images"]
if showImages:
	output = Image.fromarray(image)
	output = output.convert('RGB')
	path = os.path.dirname(args["image"]) + "/preprocessed/" + os.path.basename(args["image"])
	print("Saving preporcc result to %s" % path)
	output.save(path)
	
images.plot_rgb(image, "Preprocessed Image", showImages)

#areas = detect_text.text_areas(image)
min_conf = 0.0
#detect_text.show_areas(image, areas, min_conf)

t.start()

result, text = ocr.run(image, showImages)
ocr.print_results(image, result, showImages)

t.stop()