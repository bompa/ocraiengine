# README #

AI python project for image to text OCR.

### What is this repository for? ###

* Using open cv and tesseact v4 (deep learning model) to implement ocr text reader

### Setup (macOS) ###

* brew install anaconda
* echo "export PATH=/usr/local/anaconda3/bin:$PATH" >> ~/.zshrc # Optinal. Adds anconda python to your path in your shell startup script (chanbe zshrc to your shell startup script)
* pip install opencv-contrib-python
* brew install tesseract
