import images.image_helpers as images
import pytesseract
import cv2
import os.path
from util.timer import Timer


def run(image, showImages):

    orig = image.copy()
    (origH, origW) = image.shape[:2]
    t = Timer()

    results = []

    # apply padding to each side of the bounding box, respectively
    startX = 0
    startY = 0
    endX = origW
    endY = origH
    # extract the actual padded ROI
    roi = image[startY:endY, startX:endX]

    # in order to apply Tesseract v4 to OCR text we must supply
	# (1) a language, (2) an OEM flag of 1, indicating that the we
	# wish to use the LSTM neural net model for OCR
    config = "-l swe --oem 1 --psm 4"
    if showImages:
        config += " -c tessedit_write_images=true"
    t.start()
    text = pytesseract.image_to_string(roi, config=config)
    t.stop()
    results.append(((startX, startY, endX, endY), text))

    # tessedit_write_images writes 'tessinput.tif', which is the result of Tesseract internal pre-processing
    if showImages and os.path.isfile('tessinput.tif'):
        tessImage = cv2.imread('tessinput.tif')
        images.plot_rgb(tessImage, "Tess Input", showImages)


    # sort the results bounding box coordinates from top to bottom
    results = sorted(results, key=lambda r: r[0][1])
    return results, text

def isEmptyLine(line: str):
    if line.count(' ') == len(line):
        return True

    return False

def clean_text(text: str):
    res = "".join([(line + '\n') if not isEmptyLine(line) else "" for line in text.splitlines()])

    return res

    
def print_results(image, results, showImages = False):
    for ((startX, startY, endX, endY), text) in results:
        # display the text OCR'd by Tesseract
        print("OCR TEXT")
        print("========")
        print("{}\n".format(text))
        # strip out non-ASCII text so we can draw the text on the image
        # using OpenCV, then draw the text and a bounding box surrounding
        # the text region of the input image
        text = "".join([c if ord(c) < 128 else "" for c in text]).strip()
        output = image.copy()
        cv2.rectangle(output, (startX, startY), (endX, endY),
                (0, 0, 255), 2)
        cv2.putText(output, text, (startX, startY - 20),
                cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 255), 3)

        images.plot_rgb(output, "Result", showImages)
